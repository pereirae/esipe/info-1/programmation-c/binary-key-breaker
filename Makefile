NAME		= BinaryBreaker


#COMPILER
CC			= gcc
FLAGS		= -Wall -pedantic

#FOLDERS
SRC			= ./src
OBJ			= ./bin

#OBJ FILES
TO_BREAK	= code.o
OBJS		= $(OBJ)/main.o $(OBJ)/key.o


$(NAME): create_obj_dir $(OBJS)
	$(CC) -o $@ $(OBJS) $(TO_BREAK) $(FLAGS)

##Create the obj dir if needed
create_obj_dir:
	mkdir -p $(OBJ)

$(OBJ)/main.o: $(SRC)/main.c

$(OBJ)/key.o: $(SRC)/key.c

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) -c $< -ansi $(FLAGS) -o $@

clean:
	rm -rf $(OBJS)

fclean: clean
	rm -rf $(NAME)

re: fclean $(NAME)

.PHONY: all clean fclean re
