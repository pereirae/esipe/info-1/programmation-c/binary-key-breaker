#ifndef KEY_h_
#define KEY_h_

#include <stdlib.h>
#include <stdio.h>
#include "code.h"

typedef struct key {
  Bitkey *bitkey;
  float fitness;
} Key;


Key *allocate_key();
Bitkey *generate_random_bitkey();
Bitkey *mate_bitkeys(Bitkey *a, Bitkey *b, Bitkey *c);
void set_bit(uint8_t *bytes, int bit);
int key_comparator(const void *a, const void *b);
void destroy_key(Key *key);
void destroy_key_array(Key **key_array);

#endif
