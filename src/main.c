#include <time.h>
#include "../include/key.h"


int key_comparator(const void *a, const void *b) {
    Key **a_key;
    Key **b_key;

    a_key = (Key**)a;
    b_key = (Key**)b;
    if ((*a_key)->fitness == (*b_key)->fitness) {
        return 0;
    }
    if ((*a_key)->fitness > (*b_key)->fitness) {
        return 1;
    }
    return -1;
}

void generate_key(Key *key, int d) {
  int i;
  Key **key_tab = NULL;

  /* Retourne une clé aléatoire */
  if (d == 0) {
    key->bitkey = generate_random_bitkey();
    key->fitness = fitness_key(key->bitkey);
    return;
  }

  /* Allocation d'un tableau de clé */
  if ((key_tab = malloc(sizeof(Key*) * NB_OCT)) == NULL) {
    return;
  }
  /* Génération des clés du tableau */
  for (i = 0; i < NB_OCT; i ++) {
    if ((key_tab[i] = allocate_key()) == NULL) {
      return;
    }
    generate_key(key_tab[i], d - 1);
  }

  /* Tri du tableau */
  qsort(key_tab, NB_OCT, sizeof(Key*), key_comparator);

  /* Accouplement des trois meilleures clés */
  key->bitkey = mate_bitkeys(key_tab[NB_OCT - 1]->bitkey, key_tab[NB_OCT - 2]->bitkey, key_tab[NB_OCT - 3]->bitkey);
  key->fitness = fitness_key(key->bitkey);

  /* Libération mémoire du tableau de clé */
  destroy_key_array(key_tab);
}

int main(int argc, char **argv) {
  int d;
  Key *key = NULL;

  srand(time(NULL));

  if ((key = allocate_key()) == NULL) {
    return 1;
  }
  d = 0;
  do {
    /* Supprime la clé du tour de boucle précédent */
    if (key->bitkey != NULL) {
      free(key->bitkey);
    }
    generate_key(key, d);
    printf("Generation : %d\nKey's fitness : %f\n\n", d, key->fitness);
    d++;
  } while (key->fitness < 100.0);
  enter_the_matrix(key->bitkey);
  destroy_key(key);
  return 0;
}
