#include "../include/key.h"

Key *allocate_key() {
  Key *key = NULL;
  if ((key = malloc(sizeof(Key))) == NULL) {
    return NULL;
  }
  key->bitkey = NULL;
  key->fitness = -1.0;
  return key;
}

Bitkey *generate_random_bitkey() {
  int i;
  Bitkey *bitkey = NULL;

  if ((bitkey = malloc(sizeof(Bitkey))) == NULL) {
    return NULL;
  }

  for (i = 0; i < NB_OCT; i++) {
      bitkey->values[i] = rand() % 255;
  }
  return bitkey;
}

Bitkey *mate_bitkeys(Bitkey *a, Bitkey *b, Bitkey *c) {
  int i;
  int j;
  int bit_sum;
  Bitkey *bitkey = NULL;

  if ((bitkey = malloc(sizeof(Bitkey))) == NULL) {
    return NULL;
  }
  for (i = 0; i < NB_OCT; i++) {
    bitkey->values[i] = 0;
    for (j = 0; j < sizeof(uint8_t) * 8; j++) {
      bit_sum = 0;
      bit_sum += (a->values[i]&(1<<j)) ? 1 : 0;
      bit_sum += (b->values[i]&(1<<j)) ? 1 : 0;
      bit_sum += (c->values[i]&(1<<j)) ? 1 : 0;
      if (bit_sum > 1) {
        set_bit(&bitkey->values[i], j);
      }
    }
  }
  return bitkey;
}

void set_bit(uint8_t *bytes, int bit) {
    *bytes = (*bytes)|(1<<bit);
}

void destroy_key(Key *key) {
  if (key == NULL) {
    printf("Error : can't destroy, key is NULL\n");
    return;
  }
  free(key->bitkey);
  free(key);
}

void destroy_key_array(Key **key_array) {
  int i;
  
  for (i = 0; i < NB_OCT; i++) {
    destroy_key(key_array[i]);
  }
  free(key_array);
}
